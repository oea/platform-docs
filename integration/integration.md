# integration

network diagram of the environment of the OEA platform

The **discover** element of the [OEA platform](../architecture) is embedded in an environment consisting of
repositories on the input side and clients operated by users on the output side. Additionally machines are
also able to utilize the platform as clients.

Repositories supply metadata of digital objects stored within to the OEA platform index.
This index is searchable by clients. Search results reveal digital objects stored in repositories.

## workflow

Digital objects (image, audio, video, text) are either utilized within learning management systems (LMS)
and later on stored in repositories, or directly ingested in repositories.
The metadata of digital objects is harvested by the OEA platform and processed to a searchable index.
This index can be used by people and machines alike to locate digital objects.
Further developed digital objects can then be reintroduced to a cycle of OER.

