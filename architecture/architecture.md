# architecture

network diagram of the basic structure of the OEA platform

Applications (**webserver**, **stats**, **index**, .. **metaingest**) are represented as blocks
representing container technology for easy engineering, deployment and scalability.
Technology to be used is noted inside and at the upper right corners of the container blocks.

## discover

Clients (mobile, laptop, desktop) can request the *search* part of the **webserver** application.
The *search* is crafted to allow calls from the client to the **index** application via the **webserver**.

Clients (servers) also might utilize direct requests to the **index** via the *reverse proxy* part of the **webserver**.

Metadata can be uploaded to the platform via the **metauploadjson** application.
The **metaharvest** application is configured to fetch metadata directly from repositories or indirectly through **metauploadjson**.
The **mq** application is feed with metadata by **metaharvest** for subsequent processing.
The **metaingest** application is configured to fetch and parse metadata from the **mq** to the **index**.

Hence, the repositories provide digital objects whose metadata is then provided thru the **index** application.

The **stats** application enables for statistical analysis of **index** data,
whilst the **monitor** application facilitates command, control and monitor access to the **index**.

