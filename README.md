# documents regarding the OEA platform

## architecture

* [architecture.graphml](architecture/architecture.graphml): network diagram (created with [yEd Graph Editor](https://www.yworks.com/products/yed))
* [architecture.md](architecture/architecture.md): information on network diagram
* [architecture.pdf](architecture/architecture.pdf): PDF export of network diagram

## integration

* [integration.graphml](integration/integration.graphml): network diagram (created with [yEd Graph Editor](https://www.yworks.com/products/yed))
* [integration.md](integration/integration.md): information on network diagram
* [integration.pdf](integration/integration.pdf): PDF export of network diagram

## OEA

Visit [https://openeducation.at/](https://openeducation.at/) for more information on OEA / Open Education Austria.

